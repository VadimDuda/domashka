wizard_list = 'Паучьи лапки, жабий палец, глаз тритона, крыло летучей мыши, жир слизня, перхоть змеи'
print(wizard_list)
wizard_list = ['Паучьи лапки', 'жабий палец', 'глаз тритона', 'крыло летучей мыши', 'жир слизня', 'перхоть змеи']
print(wizard_list)
print(wizard_list[2])
wizard_list[2] = 'язык_улитки'
print(wizard_list)
print(wizard_list[2:5])
some_numbers = (1, 2, 5, 10, 20)
some_strings = ('Нож', 'отточен', 'точен', 'очень')
numbers_and_strings = [7, 'раз', 'отпей', 1, 'раз', 'отъешь']
print(numbers_and_strings)
numbers = [1, 2, 3, 4, 5]
strings = ['хватит', 'циферки', 'считать']
mylist = [numbers, strings]
print(mylist)
wizard_list.append('медвежий коготь')
print(wizard_list)
wizard_list.append('мандрагора')
wizard_list.append('болиголов')
wizard_list.append('болотный глаз')
print(wizard_list)
del wizard_list[5]
print(wizard_list)
del wizard_list[8]
del wizard_list[7]
del wizard_list[6]
print(wizard_list)
list1 = [1, 2, 3, 4, 5]
list2 = ['я', 'забрался', 'под', 'кровать']
print(list1 + list2)
list1 = [1, 2, 3, 4]
list2 = ['я', 'мечтаю', 'о', 'пломбире']
list3 = list1 + list2
print(list3)
list1 = [1, 2]
print(list1 * 5)
fibs = (0, 1, 1, 2, 3)
print(fibs[3])
favorite_sports = {
    'Ральф Уильям': 'Футбол', 
    'Майкл Типпетт': 'Баскетбол', 
    'Эдвард Элгар': 'Бейсбол', 
    'Ребекка Кларк': 'Нетбол', 
    'Этель Смит': 'Бадминтон', 
    'Фрэнк Бридж': 'Регби'
}
print(favorite_sports['Ребекка Кларк'])
del favorite_sports['Этель Смит']
print(favorite_sports)
favorite_sports['Ральф Уильям'] = 'Хоккей на льду'
print(favorite_sports)
games = 'Баскетбол', 'Страйкбол', 'Охота'
print(games)
foods = 'Драники', 'Роллы', 'Борщ'
print(foods)
print(games + foods)
favorites = games + foods
print(favorites)

homes = 3
ninjas_at_krisha = 25
tunnels = 2
samurais = 40

total_ninjas = homes * ninjas_at_krisha
total_samurais = tunnels * samurais
total_warriors = total_ninjas + total_samurais
print('Всего воинов: ' + str(total_warriors))

name = 'Привет,Vadim'
surname = 'Duda!'
print(name + surname)